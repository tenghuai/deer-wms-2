package ${basePackage}.service;

import ${basePackage}.model.${modelNameUpperCamel};
import com.deer.wms.common.core.service.Service;


/**
 * Created by ${author} on ${date}.
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}, ${type}> {

}
